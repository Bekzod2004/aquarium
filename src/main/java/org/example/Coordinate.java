package org.example;

import lombok.Getter;


@Getter
public class Coordinate {
    private int x;
    private int y;
    private int z;
    private final Volume volume;


    public Coordinate(Volume volume) {
        this.volume = volume;
        x = (int) (100000 * Math.random() % (volume.getX() / 2.0 + 1));
        y = (int) (100000 * Math.random() % (volume.getY() / 2.0 + 1));
        z = (int) (100000 * Math.random() % (volume.getZ() / 2.0 + 1));
    }
    public void getRandomMovement() {
        int xDelta = (int) (10 * Math.random() % 2);
        int yDelta = (int) (10 * Math.random() % 2);
        int zDelta = (int) (10 * Math.random() % 2);

        if ((x == volume.getX() / 2 && xDelta > 0)
                || x == -volume.getX() / 2 && xDelta < 0)
            x -= 2 * x;
        else
            x += xDelta;

        if ((y == volume.getY() / 2 && yDelta > 0)
                || y == -volume.getY() / 2 && yDelta < 0)
            y -= 2 * y;
        else y += yDelta;

        if ((z == volume.getZ() / 2 && zDelta > 0)
                || z == -volume.getZ() / 2 && zDelta < 0)
            z -= 2 * z;
        else z += zDelta;
    }

    @Override
    public int hashCode() {
        return ("" + x + y + z).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Coordinate) {
            Coordinate c = (Coordinate) obj;
            return ("" + x + y + z).equals("" + c.x + c.y + c.z);
        } else throw new IllegalArgumentException("Non Coordinate object");
    }

    @Override
    public String toString() {
        return ("(" + x + ", " + y + ", "  + z +")");
    }
}
