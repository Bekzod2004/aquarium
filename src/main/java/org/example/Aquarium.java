package org.example;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicReference;

public class Aquarium {

    public final Volume volume;

    private final int capacity;

    public final AtomicReference<LinkedList<Fish>> fishes = new AtomicReference<>(new LinkedList<>());
    private final boolean[][][] locations;

    public Aquarium(int capacity) {
        this.capacity = capacity;
        this.volume = new Volume(400,400,400);
        this.locations = new boolean[volume.getX() + 1][volume.getY() + 1][volume.getZ() + 1];
    }

    public void startWorking() {
        int males = (int) (Math.random() * 10) + 11;
        int females = (int) (Math.random() * 10) + 11;

        for (int i = 0; i < males; i++) {
            Fish child = new Fish(true, new Coordinate(volume), Fish.getRandomMaxAge(this), this);
            putFish(child);
            if (!child.isInterrupted())
                child.start();
        }

        for (int i = 0; i < females; i++) {
            Fish child = new Fish(false, new Coordinate(volume), Fish.getRandomMaxAge(this), this);
            putFish(child);
            if (!child.isInterrupted())
                child.start();
        }
    }

    public synchronized void putFish(Fish fish) {
        if (fishes.get().size() > capacity) {
            System.out.println("\nAquarium is filled with fishes");
            System.exit(1);
        }else {
            fishes.get().add(fish);
        }
    }

    public synchronized void changeFishLocation(Fish fish) {
        Coordinate coordinate = fish.getCoordinate();
        locations[coordinate.getX() + volume.getX() / 2][coordinate.getY() + volume.getY() / 2][coordinate.getZ() + volume.getZ() / 2] = false;
        coordinate.getRandomMovement();
        checkForProcreation(fish);
    }

    private synchronized void checkForProcreation(Fish fish) {
        Coordinate coordinate = fish.getCoordinate();
        int x = coordinate.getX() + volume.getX() / 2;
        int y = coordinate.getY() + volume.getY() / 2;
        int z = coordinate.getZ() + volume.getZ() / 2;
        if (!(x ==0 && y == 0 && z == 0) && !locations[x][y][z])
            locations[x][y][z] = true;
        else {
            Fish searched = null;
            for (Fish fish1 : fishes.get())
                if (fish1.getCoordinate().equals(fish.getCoordinate()) && !fish1.equals(fish)) {
                    searched = fish1;
                    break;
                }
            if (searched != null)
                fish.procreate(searched);
            changeFishLocation(fish);
        }
    }

    public synchronized void removeFish(Fish fish) {
        Coordinate coordinate = fish.getCoordinate();
        int x = coordinate.getX() + volume.getX() / 2;
        int y = coordinate.getY() + volume.getY() / 2;
        int z = coordinate.getZ() + volume.getZ() / 2;
        locations[x][y][z] = false;
        fishes.get().remove(fish);
    }
}