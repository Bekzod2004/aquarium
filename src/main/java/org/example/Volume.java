package org.example;


import lombok.Getter;

@Getter
public class Volume {
    private final int x;
    private final int y;
    private final int z;
    public Volume(int x, int y, int z) {
        if(x < 1 || y < 1 || z < 1)
            throw new IllegalArgumentException("Given values are not proper for Volume object");
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public int getVolume(){
        return x * y * z;
    }
}
