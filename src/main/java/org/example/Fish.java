package org.example;

import lombok.Getter;

import java.util.Random;

@Getter
public class Fish extends Thread {
    private final boolean isMale;
    private final Coordinate coordinate;
    private final int maxAge;
    private final Aquarium aquarium;

    public static int getRandomMaxAge(Aquarium aquarium) {
        int num = aquarium.fishes.get().size();
        return (int) Math.abs(120 - Math.random()*(num/50.) - Math.random()*100);
    }

    public Fish(boolean isMale, Coordinate coordinate, int maxAge, Aquarium aquarium) {
        this.isMale = isMale;
        this.coordinate = coordinate;
        this.maxAge = maxAge;
        this.aquarium = aquarium;
    }

    @Override
    public void run() {
        long death = System.currentTimeMillis() + 1000L * maxAge;
        while (System.currentTimeMillis() < death) {
            try {
                if (isInterrupted())
                    break;
                else sleep(aquarium.fishes.get().size()/100);
            } catch (InterruptedException e) {
                break;
            }
            move();
        }
        aquarium.removeFish(this);
        System.err.println(this + "   DEAD");
    }

    public void move() {
        aquarium.changeFishLocation(this);
    }

    public void procreate(Fish fish) {
        if (isMale != fish.isMale) {
            int childrenCount = 7 - new Random().nextInt() % (1+aquarium.fishes.get().size()/40);
            for (int i = 0; i < childrenCount; i++) {
                Fish child = new Fish(new Random().nextBoolean(), new Coordinate(aquarium.volume), getRandomMaxAge(aquarium), this.aquarium);
                aquarium.putFish(child);
                child.start();
            }
            System.out.println(this + "  AND  " + fish + "     BIRTH RATE - " + childrenCount);
        } else
            System.out.println(this + "  AND  " + fish + "    are THE SAME GENDER");
    }

    @Override
    public String toString() {
        return this.getName() +" " + coordinate + " - " + (isMale ? "Male" : "Female");
    }

}
